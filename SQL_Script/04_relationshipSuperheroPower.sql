USE SuperherosDb
GO

CREATE TABLE SuperheroPower
(
	Superhero_Id INT NOT NULL FOREIGN KEY REFERENCES Superhero(Superhero_Id),
	Power_Id INT NOT NULL FOREIGN KEY REFERENCES Power(Power_Id),
	CONSTRAINT PK_SuperheroPower PRIMARY KEY ([Superhero_Id],[Power_Id])
);
