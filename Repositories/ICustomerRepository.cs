﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment6.Models;

namespace Assignment6.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomerById(int id);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetCountCustomersByCountry();

        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomerById(Customer customer);
        public List<Customer> GetCustomerByName(string firstName);
        public List<Customer> GetLimitedcustomers(int limit, int offset);
    }
}
