﻿using Assignment6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Repositories
{
    class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Adding new customer to the Customer Table inthe Chinook Database
        /// </summary>
        /// <param name="customer"></param>
        /// <returns> Integer value </returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool customerAdded = false;
            string sql = "INSERT INTO Customer (FirstName,LastName,Country,Postalcode,Phone,Email)" +
                "Values(@firstName,@lastName,@country,@postalCode,@PhoneNumber,@Email)";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@firstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@lastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@country", customer.Country);
                        cmd.Parameters.AddWithValue("@postalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@phoneNumber", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        customerAdded = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customerAdded;
        }
        /// <summary>
        /// Fetching all customers from the Customer Table in Chinook Database
        /// </summary>
        /// <returns>List of Customers</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();

            string sql = "SELECT CustomerId,FirstName,LastName,Country,ISNULL(PostalCode,'No-PostalCode'),ISNULL(Phone,'No-Phone'),Email " +
                              "as PostalCode,Phone,Email FROM Customer ";



            try
            {
                //Connect
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();

                    //Make a command

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //REader

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Handle REsult
                            // SqlString descriptionValue = SqlString.Null;

                            while (reader.Read())
                            {

                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.PhoneNumber = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            catch (NullReferenceException e)
            {
                Console.Write(e.Message);
            }
            catch (SqlNullValueException e)
            {
                Console.WriteLine(e.Message);
            }
            return custList;
        }
        /// <summary>
        /// Groups the customers by country
        /// </summary>
        /// <returns>List of customers </returns>
        public List<Customer> GetCountCustomersByCountry()
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT COUNT(CustomerId) as NoOfCustomersByCountry, Country FROM Customer GROUP BY Country ORDER BY Country DESC ";
            try
            {
                //Connect
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();

                    //Make a command

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //REader

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Handle REsult

                            while (reader.Read())
                            {

                                Customer temp = new Customer();
                                temp.NoOfCustomersByCountry = reader.GetInt32(0);
                                temp.Country = reader.GetString(1);
                                customers.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            catch (NullReferenceException e)
            {
                Console.Write(e.Message);
            }
            catch (SqlNullValueException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
        /// <summary>
        /// Returns a specific customer as per given Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns> Specific Customer</returns>
        public Customer GetCustomerById(int id)
        {

            string sql = "SELECT FirstName,LastName,Country,ISNULL(PostalCode,'No-PostalCode') " +
                         "as PostalCode,Phone,Email FROM Customer WHERE CustomerId=@CustomerId ";
            Customer temp = new Customer();

            try
            {
                //Connect
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();

                    //Make a command

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //REader
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Handle REsult
                            while (reader.Read())
                            {
                                temp.CustomerId = id;
                                temp.FirstName = reader.GetString(0);
                                temp.LastName = reader.GetString(1);
                                temp.Country = reader.GetString(2);
                                temp.PostalCode = reader.GetString(3);
                                temp.PhoneNumber = reader.GetString(4);
                                temp.Email = reader.GetString(5);
                            }

                        }
                    }

                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            catch (NullReferenceException e)
            {
                Console.Write(e.Message);
            }
            catch (SqlNullValueException e)
            {
                Console.WriteLine(e.Message);
            }
            return temp;
        }
        /// <summary>
        /// To get specific customer By name
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns>Single customer if the given name matches</returns>
        public List<Customer> GetCustomerByName(string firstName)
        {
            string sql = "SELECT CustomerId,FirstName,LastName, Country, ISNULL(PostalCode, 'No-PostalCode') " +
                         "as PostalCode,Phone,Email FROM Customer WHERE FirstName Like @firstName";

            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@firstName", '%' + firstName + '%');
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.PhoneNumber = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customers.Add(temp);
                            }


                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
        /// <summary>
        /// To get the Limited customers as per given limit value and Offset value
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>group of customers as per limit and offset value</returns>
        public List<Customer> GetLimitedcustomers(int limit, int offset)
        {
            List<Customer> custList = new List<Customer>();

            string sql = "SELECT CustomerId,FirstName,LastName,Country,ISNULL(PostalCode,'No-PostalCode') " +
                         "as PostalCode,Phone,Email FROM Customer ORDER BY CustomerId desc offset @Offset rows fetch next @Limit rows only  ";
            try
            {
                //Connect
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();

                    //Make a command

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        //REader
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Handle REsult
                            while (reader.Read())
                            {

                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.PhoneNumber = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            catch (NullReferenceException e)
            {
                Console.Write(e.Message);
            }
            catch (SqlNullValueException e)
            {
                Console.WriteLine(e.Message);
            }
            return custList;
        }

        /// <summary>
        /// Updates the perticular customer as per given Id
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>returns an integer</returns>
        public bool UpdateCustomerById(Customer customer)
        {
            bool customerUpdated = false;
            string sql = "UPDATE Customer SET FirstName=@firstName,LastName=@lastName," +
                "Country=@country,Postalcode=@postalCode,Phone=@PhoneNumber,Email=@Email WHERE CustomerId=@customerId";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@customerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@firstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@lastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@country", customer.Country);
                        cmd.Parameters.AddWithValue("@postalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@phoneNumber", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        customerUpdated = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customerUpdated;
        }
    }
}
