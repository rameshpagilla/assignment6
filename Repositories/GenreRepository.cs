﻿using Assignment6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Repositories
{
    public class GenreRepository : IGenre
    {
        /// <summary>
        /// To get Customers with Most popular Genre  
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns>List of customers with popular genre</returns>
        public List<Genre> CustomerMostPopularGenre(string firstName)
        {
            List<Genre> CustomerPopularGenre = new List<Genre>();
            string sql = "SELECT TOP 1 WITH TIES Customer.FirstName , Genre.Name as GenreName, COUNT(Genre.Name) as GenreCount " +
                " FROM Customer INNER JOIN Invoice ON Customer.CustomerID = Invoice.CustomerID" +
                " INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId" +
                "  INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId" +
                " INNER JOIN Genre ON Track.GenreId = Genre.GenreId" +
                " WHERE Customer.FirstName = @firstName " +
                " GROUP BY Genre.Name, Customer.FirstName" +
                " ORDER BY GenreCount DESC;";
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@firstName", firstName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Genre temp = new Genre();
                                temp.CustomerFirstName = reader.GetString(0);
                                // temp.CustomerLastName = reader.GetString(1);
                                temp.GenreName = reader.GetString(1);
                                temp.GenreCount = reader.GetInt32(2);

                                CustomerPopularGenre.Add(temp);

                            }
                        }

                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return CustomerPopularGenre;
        }
    }
}
