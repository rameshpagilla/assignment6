# Assignment 6
- Data Access with SQL Client

**Group Work of** : Ramesh Pagilla and Mosiur Rahman

## Technologies
- This is an assignment code to perform basic CRUD operations using C# and .Net Core

## Running the Code
 
- Clone project 
 
- Open the project in VS
 
- Open Program.cs

- Remove comment from the method you want to run and  click Run 



