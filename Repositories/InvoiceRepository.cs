﻿using Assignment6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Repositories
{
    class InvoiceRepository : IInvoiceRepository
    {
        /// <summary>
        /// To Get Highest Spenders from Invoice and Customer tables in Chinook DB
        /// </summary>
        /// <returns>returns the list of highest Spenders</returns>
        public List<Invoice> GetHighestSpenders()
        {
            List<Invoice> customers = new List<Invoice>();
            string sql = "SELECT   Customer.FirstName, Customer.LastName, SUM(Invoice.Total) as Total FROM Customer INNER JOIN Invoice ON " +
                "  Customer.CustomerId = Invoice.CustomerId GROUP BY  Customer.FirstName, Customer.LastName ORDER BY Total DESC";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Invoice temp = new Invoice();
                                temp.FirstName = reader.GetString(0);
                                temp.LastName = reader.GetString(1);
                                temp.Total = reader.GetDecimal(2);
                                customers.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
    }
}
